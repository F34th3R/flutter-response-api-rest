import 'package:flutter/material.dart';
import 'package:flutter_request_app/views/home/home_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
            debugShowCheckedModeBanner: false,
            title: "WhatsApp",
            theme: new ThemeData(
                brightness: Brightness.light,
                primaryColor: Colors.lightBlue[800],
                accentColor: Colors.cyan[600],
            ),
            home: new HomePage(),
        );
    }
}