import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() {
    return new _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {

    List data;
    Future<String> getData() async {
        http.Response response = await http.get(
            Uri.encodeFull("http://jsonplaceholder.typicode.com/photos"),
            headers: {
//                "key": "myKey",
                "Accept": "application/json",
            },
        );
        this.setState(() {
            data = JSON.decode(response.body);
        });
        print(data[1]["title"]);
    }
    @override
  void initState() {
        this.getData();
  }

    @override
      Widget build(BuildContext context) {
        return new Scaffold(
            appBar: new AppBar(
                title: new Text("Request Data"),
            ),
            body: new ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, i) {
                    return new Card(
                        child: new Container(
                            padding: const EdgeInsets.all(15.0),
                            child: new Text(data[i]["title"]),
                        ),
                        margin: const EdgeInsets.all(5.0),
                    );
                },
            ),
        );
      }
}